//
//  LoginViewController.swift
//  Empresas ioasys
//
//  Created by Elis Nunes on 07/05/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var maskHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundBottonConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var bemVindoWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bemVindoLabel: UILabel!
    @IBOutlet weak var maskImageView: UIImageView!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var entrarButton: UIButton!
    
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var senhaErrorLabel: UILabel!
    
    @IBOutlet weak var emailErrorButton: UIButton!
    @IBOutlet weak var senhaErrorButton: UIButton!
    @IBOutlet weak var verSenhaButton: UIButton!
    
    var loaderView: UIView!
    
    // MARK: - Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        backgroundBottonConstraint.constant = 34.0;
        
        entrarButton.layer.cornerRadius = 4.0;
        
        ajustesIniciais();
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?)
    {
        ajustaMask();
    }

    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator)
    {
        ajustaMask();
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated);
        
        bemVindoWidthConstraint.constant = view.frame.size.width;
        UIView.animate(withDuration: 1.0, animations:
        {
            self.view.layoutIfNeeded();
        }, completion:
        {res in
            self.animaBackground(comTeclado: false);
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated);
        
        if((UserDefaults.standard.string(forKey: "access-token")) != nil)
        {
            OperationQueue.main.addOperation
            {
                [weak self] in
                self?.performSegue(withIdentifier: "Pesquisa Segue", sender: nil);
            }
            
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        animaBackground(comTeclado: false);
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
    }
    
    // MARK: - Private Functions
    
    func ajustesIniciais()
    {
        emailTextField.delegate = self;
        senhaTextField.delegate = self;
        emailTextField.setPaddingPoints(12.0);
        senhaTextField.setPaddingPoints(12.0);
        limpaErro(campo: emailTextField, campoError: emailErrorLabel, buttonError: emailErrorButton);
        limpaErro(campo: senhaTextField, campoError: senhaErrorLabel, buttonError: senhaErrorButton);
        
        self.verSenhaButton.isHidden = true;
        
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing));
        tap.cancelsTouchesInView = true;
        view.addGestureRecognizer(tap);
        
        ajustaMask();
    }
    
    func ajustaMask()
    {
        if traitCollection.userInterfaceStyle == .dark
        {
            maskImageView.image = UIImage.init(named: "mask-dark");
        }
        else
        {
            maskImageView.image = UIImage.init(named: "mask");
        }
    }
    
    func animaBackground(comTeclado: Bool)
    {
        
        let h: CGFloat = view.frame.size.height/1.5;
        
        UIView.animate(withDuration: 0.5, animations:
        {
            if comTeclado
            {
                self.logoCenterConstraint.constant = 15;
                self.bemVindoLabel.alpha = 0.0;
                self.bemVindoWidthConstraint.constant = 0;
                self.logoWidthConstraint.constant = 60.0;
                self.backgroundBottonConstraint.constant = CGFloat(-(h+120));
            }
            else
            {
                self.logoCenterConstraint.constant = 0;
                self.bemVindoLabel.alpha = 1.0;
                self.bemVindoWidthConstraint.constant = self.view.frame.size.width;
                self.logoWidthConstraint.constant = 185.0;
                self.backgroundBottonConstraint.constant = CGFloat(-h);
            }
            self.view.layoutIfNeeded();
        });
    }
    
    func entrar()
    {
        view.endEditing(true);
        
//        if emailTextField.text == "t" // usado para teste pra agilizar a digitação de credenciais validas
//        {
//            emailTextField.text = "testeapple@ioasys.com.br";
//            senhaTextField.text = "12341234";
//        }
        
        limpaErro(campo: emailTextField, campoError: emailErrorLabel, buttonError: emailErrorButton);
        limpaErro(campo: senhaTextField, campoError: senhaErrorLabel, buttonError: senhaErrorButton);
        
        if emailTextField.text == ""
        {
            exibeErro(mensagem: "Informe seu E-mail", campo: emailTextField, buttonError: emailErrorButton, error: emailErrorLabel);
            return;
        }
        if !isValidEmail(email: emailTextField.text ?? "")
        {
            exibeErro(mensagem: "E-mail inválido", campo: emailTextField, buttonError: emailErrorButton, error: emailErrorLabel);
            return;
        }
        if senhaTextField.text == ""
        {
            exibeErro(mensagem: "Informe sua Senha", campo: senhaTextField, buttonError: senhaErrorButton, error: senhaErrorLabel);
            return;
        }
        
        mostraLoader();
        
        API().login(email: emailTextField.text!, senha: senhaTextField.text!)
        { (json) in
            DispatchQueue.main.async {
                
                self.escondeLoader();
                
                if (json["success"] as? Bool)!
                {
                    self.emailTextField.text = "";
                    self.senhaTextField.text = "";
                    self.performSegue(withIdentifier: "Pesquisa Segue", sender: self);
                }
                else
                {
                    self.exibeErro(mensagem: "", campo: self.emailTextField, buttonError: self.emailErrorButton, error: self.emailErrorLabel);
                    self.exibeErro(mensagem: "Credenciais inválidas", campo: self.senhaTextField, buttonError: self.senhaErrorButton, error: self.senhaErrorLabel);
                }
            }
        };
    }
    
    func mostraLoader()
    {
        loaderView = UINib(nibName: "LoaderView", bundle: .main).instantiate(withOwner: nil, options: nil).first as? UIView;
        loaderView.isHidden = false;
        loaderView.frame = view.bounds
        view.addSubview(loaderView);
    }
    
    func escondeLoader()
    {
        loaderView.isHidden = true;
        loaderView = nil;
    }
    
    func limpaErro(campo textField:UITextField, campoError label:UILabel, buttonError button:UIButton)
    {
        textField.layer.borderColor = nil;
        textField.layer.borderWidth = 0.0;
        textField.layer.cornerRadius = 4.0;
        label.text = "";
        button.isHidden = true;
        self.verSenhaButton.isHidden = true;
    }
    
    func exibeErro(mensagem msg:String, campo textField:UITextField, buttonError button:UIButton, error label:UILabel)
    {
        label.text = msg;
        textField.layer.borderColor = CGColor.init(srgbRed: 235.0/255.0, green: 87.0/255.0, blue: 87.0/255.0, alpha: 1.0);
        textField.layer.borderWidth = 1.0;
        textField.becomeFirstResponder();
        button.isHidden = false;
        self.verSenhaButton.isHidden = true;
    }
    
    func isValidEmail(email: String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // MARK: - Target Actions
    
    @IBAction func entrarButton(_ sender: UIButton)
    {
        entrar();
    }
    @IBAction func limpaEmailButton(_ sender: UIButton)
    {
        limpaErro(campo: self.emailTextField, campoError: self.emailErrorLabel, buttonError: self.emailErrorButton);
    }
    @IBAction func limpaSenhaButton(_ sender: UIButton)
    {
        limpaErro(campo: self.senhaTextField, campoError: self.senhaErrorLabel, buttonError: self.senhaErrorButton);
    }
    @IBAction func verSenhaButton(_ sender: UIButton)
    {
        self.senhaTextField.isSecureTextEntry = !self.senhaTextField.isSecureTextEntry;
    }
    
    // MARK: - Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        animaBackground(comTeclado: true);
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.animaBackground(comTeclado: false);
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField.tag==1
        {
            senhaTextField.becomeFirstResponder();
        }
        if textField.tag==2
        {
            entrar();
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.tag==1
        {
            self.emailErrorButton.isHidden=true;
        }
        if textField.tag==2
        {
            self.senhaErrorButton.isHidden=true;
            self.verSenhaButton.isHidden=false;
        }
        return true;
    }
    
}
