//
//  EmpresaViewController.swift
//  Empresas ioasys
//
//  Created by Elis Nunes on 08/05/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class EmpresaViewController: UIViewController {
    
    var tituloString: String!
    var fotoImage: UIImage!
    var textoString: String!
    var corBackground: UIColor!
    
    @IBOutlet weak var voltarButton: UIButton!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var fotoImageView: UIImageView!
    @IBOutlet weak var empresaLabel: UILabel!
    @IBOutlet weak var textView: UITextView!

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        voltarButton.layer.cornerRadius = 4.0;
        
        self.tituloLabel.text = self.tituloString;
        self.empresaLabel.text = self.tituloString;
        self.fotoImageView.image = self.fotoImage;
        self.textView.text = self.textoString;
        self.baseView.backgroundColor = self.corBackground;
        
    }
    
    @IBAction func voltarButton(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil);
    }
    
    

}
