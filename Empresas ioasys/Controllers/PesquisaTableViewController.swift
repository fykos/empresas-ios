//
//  PesquisaTableViewController.swift
//  Empresas ioasys
//
//  Created by Elis Nunes on 08/05/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class PesquisaTableViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var baseBuscaView: UIView!
    @IBOutlet weak var buscaView: UIView!
    @IBOutlet weak var buscaTextField: UITextField!
    
    var loaderView: UIView!
    
    var baseCGRect: CGRect!
    
    var errorString: String!
    
    
    struct Person {
        var name: String
        var surname: String
        var identifier: String
    }
    
    struct empresa {
        var name : String!
        var photo : String!
        var text : String!
    }
    var empresasArray = [empresa]()
    var arrayDictionary = [[String:Any]]()
    
    // MARK - Private Functions
    
    func pesquisar()
    {
        self.view.endEditing(true);
        
        if buscaTextField.text == ""
        {
            self.empresasArray.removeAll();
            self.baseBuscaView.frame = baseCGRect;
            UIView.animate(withDuration: 0.5, animations:
            {
                self.view.layoutIfNeeded();
            })
            self.tableView.reloadData();
        }
        else
        {
            if self.loaderView != nil
            {
                return;
            }
            
            self.empresasArray.removeAll();
            
            mostraLoader();
            
            API().pesquisar(pesquisa: buscaTextField.text!) { (json) in
                DispatchQueue.main.async {
                    
                    self.escondeLoader();
                    
                    if let data = json as? [String: Any] {
                        
                        let array = data["enterprises"] as! Array<Any>;
                        for dic in array
                        {
                            guard let dict = dic as? [AnyHashable: Any] else {
                                print("\(dic) couldn't be converted to Dictionary")
                                return
                            }
                            
                            let name: String = dict["enterprise_name"] as! String;
                            let text: String = dict["description"] as! String;
                            var photo: String = "";
                            if !(dict["photo"] is NSNull)
                            {
                                photo = dict["photo"] as! String;
                            }
                            
                            self.empresasArray.append(empresa(name: name, photo: photo, text: text))
                        }
                    }
                    
                    self.tableView.reloadData();
                    
                    if self.empresasArray.count == 0
                    {
                        self.buscaTextField.becomeFirstResponder();
                    }
                    
                }
            };
        }
    }
    
    func mostraLoader()
    {
        self.loaderView = UINib(nibName: "LoaderView", bundle: .main).instantiate(withOwner: nil, options: nil).first as? UIView;
        self.loaderView.isHidden = false;
        self.loaderView.frame = view.bounds
        view.addSubview(self.loaderView);
    }
    
    func escondeLoader()
    {
        self.loaderView?.isHidden = true;
        self.loaderView = nil;
        
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buscaView.layer.cornerRadius = 4.0;
        
        buscaTextField.delegate = self;
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.cancelsTouchesInView = false;
        self.view.addGestureRecognizer(tap)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil)
    {
        self.buscaTextField.becomeFirstResponder();
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated);
        baseCGRect = self.baseBuscaView.frame;
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if self.empresasArray.count == 0
        {
            return 1
        }
        return self.empresasArray.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if buscaTextField.text != "" && self.empresasArray.count == 0
        {
            return 1;
        }
        if self.empresasArray.count != 0
        {
            return 1;
        }
        
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var identifier = "Nenhum Cell";
        
        if self.empresasArray.count != 0
        {
            identifier = "Empresa Cell";
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)

        return cell
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if self.empresasArray.count != 0
        {
            let cellData = self.empresasArray[indexPath.section]
            
            let fotoImageView: UIImageView = cell.viewWithTag(1) as! UIImageView;
            let tituloLabel: UILabel = cell.viewWithTag(2) as! UILabel;
            let activityIndicatorView: UIActivityIndicatorView = cell.viewWithTag(3) as! UIActivityIndicatorView;
            
            tituloLabel.text = cellData.name;
            
            activityIndicatorView.startAnimating();
            let queue = DispatchQueue.global();
            queue.async {
                let url = URL(string: "https://empresas.ioasys.com.br/api/v1"+cellData.photo)! // tentei alterar o link tirando o vi e o api mas mesmo assim nào carregou a imagem
                let data = try? Data(contentsOf: url)
                DispatchQueue.main.async {
                    if let imageData = data {
                        fotoImageView.image = UIImage(data: imageData)
                    }
                    else
                    {
                        fotoImageView.image = UIImage(named: "sem-foto")
                    }
                    activityIndicatorView.stopAnimating();
                }
            }
            
            if (indexPath.section % 2 == 0)
            {
                cell.backgroundColor = UIColor.init(cgColor: CGColor.init(srgbRed: 121.0/255.0, green: 187.0/255.0, blue: 202.0/255.0, alpha: 1.0));
            }
            else
            {
                if (indexPath.section % 3 == 0)
                {
                    cell.backgroundColor = UIColor.init(cgColor: CGColor.init(srgbRed: 236.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 1.0));
                }
                else
                {
                    cell.backgroundColor = UIColor.init(cgColor: CGColor.init(srgbRed: 143.0/255.0, green: 187.0/255.0, blue: 129.0/255.0, alpha: 1.0));
                }
            }
        }
        else
        {
            cell.backgroundColor = UIColor.clear;
        }
    }
    
    // MARK: - Target Actions
    
    @IBAction func desconectarButton(_ sender: UIButton)
    {
        UserDefaults.standard.set(nil, forKey: "access-token");
        UserDefaults.standard.set(nil, forKey: "client");
        UserDefaults.standard.set(nil, forKey: "uid");
        
        self.dismiss(animated: true, completion: nil);
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Empresa Segue"
        {
            if let empresaViewController = segue.destination as? EmpresaViewController
            {
                let indexPath: NSIndexPath = sender as! NSIndexPath;
                let cell: UITableViewCell = self.tableView.cellForRow(at: indexPath as IndexPath)!;
                
                let fotoImageView: UIImageView = cell.viewWithTag(1) as! UIImageView;
                
                let cellData = self.empresasArray[indexPath.section]
                
                empresaViewController.tituloString = cellData.name!;
                empresaViewController.fotoImage = fotoImageView.image;
                empresaViewController.textoString = cellData.text;
                empresaViewController.corBackground = cell.backgroundColor;
            }
        }
        
    }
    
    // MARK: - Delegates

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if section == 0
        {
            if self.empresasArray.count != 0
            {
                if self.empresasArray.count == 1
                {
                    return "01 resultado encontrado";
                }
                else
                {
                    if self.empresasArray.count < 10
                    {
                        return "0"+String(self.empresasArray.count)+" resultados encontrados";
                    }
                    else
                    {
                        return String(self.empresasArray.count)+" resultados encontrados";
                    }
                }
            }
        }
        return "";
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 55.0;
        }
        return 8.0;
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.empresasArray.count == 0
        {
            return 287.0;
        }
        return 120.0;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true);
        
        self.performSegue(withIdentifier: "Empresa Segue", sender: indexPath);
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.baseBuscaView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 60);
        UIView.animate(withDuration: 0.5, animations:
        {
            self.view.layoutIfNeeded();
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        pesquisar();
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        pesquisar();
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        textField.text = "";
        pesquisar();
        return true;
    }

}
