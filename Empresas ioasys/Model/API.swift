//
//  API.swift
//  Empresas ioasys
//
//  Created by Elis Nunes on 08/05/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Foundation
import UIKit

class API: NSObject {

    public let urlApi = "https://empresas.ioasys.com.br/api/v1"
    
    func login(email emailString:String, senha senhaString:String, completion: @escaping (AnyObject) -> ())
    {

        let session = URLSession.shared
        
        let todoEndpoint: String = urlApi+"/users/auth/sign_in"
        guard let url = URL(string: todoEndpoint) else {
          print("Error: cannot create URL")
          return
        }
        
        var urlRequest = URLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy , timeoutInterval: 15.0)

        let parameters = "{\"email\":\""+emailString+"\",\"password\":\""+senhaString+"\"}"
        let postData = parameters.data(using: .utf8)
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = postData
        
        let task = session.dataTask(with: urlRequest)
        {(data, response, error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse
            {
                 if let accessToken = httpResponse.allHeaderFields["access-token"] as? String
                 {
                    UserDefaults.standard.set(accessToken, forKey: "access-token");
                 }
                 if let client = httpResponse.allHeaderFields["client"] as? String
                 {
                    UserDefaults.standard.set(client, forKey: "client");
                 }
                if let uid = httpResponse.allHeaderFields["uid"] as? String
                {
                   UserDefaults.standard.set(uid, forKey: "uid");
                }
            }
            
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            completion(json as AnyObject);
        }
        task.resume()
    }
    
    func pesquisar(pesquisa pesquisaString:String, completion: @escaping (AnyObject) -> ())
    {

        let session = URLSession.shared
        
        let todoEndpoint: String = urlApi+"/enterprises?enterprise_types=5&name="+pesquisaString // coloquei o type 5 para ter resultado de empresas com foto como a Orand / Impresee
        guard let url = URL(string: todoEndpoint) else {
          print("Error: cannot create URL")
          return
        }
        
        var urlRequest = URLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 15.0)

        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue(UserDefaults.standard.string(forKey: "access-token")!, forHTTPHeaderField: "access-token")
        urlRequest.addValue(UserDefaults.standard.string(forKey: "client")!, forHTTPHeaderField: "client")
        urlRequest.addValue(UserDefaults.standard.string(forKey: "uid")!, forHTTPHeaderField: "uid")
        
        urlRequest.httpMethod = "GET"
        
        let task = session.dataTask(with: urlRequest)
        {(data, response, error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            completion(json as AnyObject);
        }
        task.resume()
    }
    
}
