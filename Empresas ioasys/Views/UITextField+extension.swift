//
//  UITextField+extension.swift
//  Empresas ioasys
//
//  Created by Elis Nunes on 08/05/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Foundation
import UIKit

extension UITextField
{
    func setPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
