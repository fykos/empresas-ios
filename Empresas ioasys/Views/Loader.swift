//
//  Loader.swift
//  Empresas ioasys
//
//  Created by Elis Nunes on 08/05/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class Loader: UIView {

    @IBOutlet weak var loader1ImageView: UIImageView!
    @IBOutlet weak var loader2ImageView: UIImageView!
    
    override func draw(_ rect: CGRect)
    {
        self.anima1();
        self.anima2();
    }
    
    func anima1()
    {
        UIView.animate(withDuration: 2.0, delay: 0.0, options: .curveLinear, animations: {
            self.loader1ImageView.transform = self.loader1ImageView.transform.rotated(by: CGFloat.pi)
        }) { finished in
            self.anima1();
        }
    }
    
    func anima2()
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveLinear, animations: {
            self.loader2ImageView.transform = self.loader2ImageView.transform.rotated(by: CGFloat.pi)
        }) { finished in
            self.anima2();
        }
    }
    
}
